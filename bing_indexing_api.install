<?php

/**
 * @file
 * Install, update and uninstall functions for the bing_indexing_api module.
 */

declare(strict_types=1);

/**
 * Implements hook_update_N().
 *
 * Updated configuration for sites that installed the module before.
 */
function bing_indexing_api_update_10001(): void {
  $state = \Drupal::state();

  if ($api_key = $state->get('bing_index_api_key')) {
    \Drupal::configFactory()->getEditable('bing_indexing_api.settings')
      ->set('node_presave', TRUE)
      ->set('node_presave_published_only', TRUE)
      ->set('node_unpublish', TRUE)
      ->set('node_delete', TRUE)
      ->set('node_delete_published_only', TRUE)
      ->save();

    \Drupal::configFactory()->getEditable('bing_indexing_api.credentials')
      ->set('api_key', $api_key)
      ->set('base_domain', $state->get('bing_index_api_base_domain') ?? '')
      ->save();

    $state->delete('bing_index_api_base_domain');
    $state->delete('bing_index_api_key');
  }

}
