<?php

declare(strict_types=1);

namespace Drupal\bing_indexing_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides Bing Settings Form.
 */
class BingSettingsForm extends ConfigFormBase {

  const CONFIG_NAME = 'bing_indexing_api.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'bing_indexing_api_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::CONFIG_NAME);

    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('URL Submission Settings'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['settings']['node_presave'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track node creation/updating'),
      '#description' => $this->t('Trigger URL submission when the node is created or updated.'),
      '#default_value' => $config->get('node_presave'),
    ];

    $form['settings']['node_presave_published_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only creation/updating of published nodes.'),
      '#description' => $this->t('Trigger URL submission when the node is created or updated, but only if the node is published. Recommended.'),
      '#default_value' => $config->get('node_presave_published_only'),
      '#states' => [
        'visible' => [
          ':input[name="node_presave"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['settings']['node_unpublish'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track node unpublishing'),
      '#description' => $this->t('Trigger URL submission when the node becomes unpublished. Recommended.'),
      '#default_value' => $config->get('node_unpublish'),
    ];

    $form['settings']['node_delete'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track node deletion'),
      '#description' => $this->t('Trigger URL submission when the node is deleted.'),
      '#default_value' => $config->get('node_delete'),
    ];

    $form['settings']['node_delete_published_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only the deletion of published nodes'),
      '#description' => $this->t('Trigger URL submission only when the deleted node is published. Recommended.'),
      '#default_value' => $config->get('node_presave_published_only'),
      '#states' => [
        'visible' => [
          ':input[name="node_delete"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config(self::CONFIG_NAME)
      ->set('node_presave', $form_state->getValue('node_presave'))
      ->set('node_presave_published_only', $form_state->getValue('node_presave_published_only'))
      ->set('node_unpublish', $form_state->getValue('node_unpublish'))
      ->set('node_delete', $form_state->getValue('node_delete'))
      ->set('node_delete_published_only', $form_state->getValue('node_delete_published_only'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
