<?php

declare(strict_types=1);

namespace Drupal\bing_indexing_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides Bing Credentials Form.
 */
class BingCredentialsForm extends ConfigFormBase {

  const CONFIG_NAME = 'bing_indexing_api.credentials';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'bing_index_api_credentials_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::CONFIG_NAME);

    $form['base_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The Base Domain'),
      '#description' => $this->t('Provide the site domain. Example: https://example.com'),
      '#default_value' => $config->get('base_domain'),
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t('Follow <a href="https://learn.microsoft.com/en-us/bingwebmaster/getting-access#using-api-key">the instruction</a> to configure Bing and get API key.'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config(self::CONFIG_NAME)
      ->set('base_domain', $form_state->getValue('base_domain'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
