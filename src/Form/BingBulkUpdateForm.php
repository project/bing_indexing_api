<?php

declare(strict_types=1);

namespace Drupal\bing_indexing_api\Form;

use Drupal\bing_indexing_api\Service\BingIndexingApiInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides BingBulkUpdateForm for URLs submission.
 */
class BingBulkUpdateForm extends FormBase {

  public function __construct(protected BingIndexingApiInterface $bingIndexingApi) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('bing_indexing_api.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'bing_index_api_bulk_update_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['urls'] = [
      '#type' => 'textarea',
      '#required' => TRUE,
      '#title' => $this->t('URLs for submission through Bing Webmaster Tools'),
      '#description' => $this->t('Provide the absolute URLs you wish to reindex in Bing, each on a new line.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $url_input = $form_state->getValue('urls');

    // Preprocess user input. Validate URLs.
    if ($url_input && is_string($url_input)) {
      $urls = array_filter(array_map('trim', explode("\n", $url_input)));

      foreach ($urls as $key => $url) {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
          unset($urls[$key]);
        }
      }
    }

    if (!empty($urls)) {
      $result = $this->bingIndexingApi->reindexUrl($urls);
      $this->messenger()->addMessage($result['message'], $result['success'] ? MessengerInterface::TYPE_STATUS : MessengerInterface::TYPE_ERROR);
    }
    else {
      $this->messenger()->addMessage($this->t('No valid URLs to reindex.'));
    }
  }

}
