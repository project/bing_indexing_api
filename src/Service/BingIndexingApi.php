<?php

declare(strict_types=1);

namespace Drupal\bing_indexing_api\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Error;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides the reindexing URL request to Bing Webmaster Tools.
 */
class BingIndexingApi implements BingIndexingApiInterface {

  use StringTranslationTrait;

  /**
   * The Bing Indexing API endpoint.
   */
  const ENDPOINT = 'https://www.bing.com/webmaster/api.svc/json/SubmitUrlbatch?apikey=';

  public function __construct(protected ConfigFactoryInterface $config, protected LoggerInterface $logger, protected ClientInterface $client) {
  }

  /**
   * {@inheritdoc}
   */
  public function reindexUrl(string|array $urls): array {
    $urls = is_string($urls) ? [$urls] : $urls;
    $success = FALSE;
    if ($response = $this->callApi($urls)) {
      $status_code = $response->getStatusCode();
      $urls = implode(', ', $urls);
      if ($success = $status_code === 200) {
        $this->logger->notice('Bing Indexing API: successfully triggered URL reindexing for: @url.', ['@url' => $urls]);
      }
      else {
        $body = Json::decode($response->getBody()->getContents());
        $body = is_array($body) ? $body : [];
        $this->logger->error('The Bing Index API returned a status code of @status_code for @url. Response message: @message', [
          '@status_code' => $status_code,
          '@url' => $urls,
          '@message' => $body['error']['message'] ?? '',
        ]);
      }
    }

    return [
      'success' => $success,
      'message' => $success ? $this->t('Successfully triggered Bing URL submission for the following URLs: @url.', ['@url' => $urls]) : $this->t('A problem occurred during the Bing URL submission for the following URLs: @url', ['@url' => $urls]),
    ];
  }

  /**
   * Requests URLs reindexing in Bing Webmaster Tools.
   *
   * @param array $urls
   *   The URLs to be reindexed in Bing Webmaster Tools.
   *
   * @return \Psr\Http\Message\ResponseInterface|null
   *   API response if successful or NULL
   */
  public function callApi(array $urls): ?ResponseInterface {
    if (empty($urls)) {
      return NULL;
    }
    $config = $this->config->get('bing_indexing_api.credentials');
    $base_domain = $config->get('base_domain');
    $api_key = $config->get('api_key');

    if (!$base_domain || !$api_key) {
      $this->logger->error('Check the Bing API settings: the domain or API key is missing.');
      return NULL;
    }

    try {
      return $this->client->request('POST', self::ENDPOINT . $api_key, [
        'json' => [
          'siteUrl' => $base_domain,
          'urlList' => $urls,
        ],
        'headers' => [
          'Content-type' => 'application/json; charset=utf-8',
        ],
      ]);
    }
    catch (\Exception | GuzzleException $exception) {
      Error::logException($this->logger, $exception, 'A problem occurred during URL reindexing: @urls. Details: @message', [
        '@message' => $exception->getMessage(),
        'urls' => implode(', ', $urls),
      ]);
    }

    return NULL;
  }

}
