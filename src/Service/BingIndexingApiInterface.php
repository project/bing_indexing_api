<?php

declare(strict_types=1);

namespace Drupal\bing_indexing_api\Service;

/**
 * Provides an interface for BingIndexingApi service.
 */
interface BingIndexingApiInterface {

  /**
   * Requests URLs reindexing in Bing Webmaster Tools.
   *
   * @param string|array $urls
   *   The URLs to be reindexed.
   */
  public function reindexUrl(string|array $urls): array;

}
