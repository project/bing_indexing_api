# Bing Indexing API

This module connects with Microsoft Bing Indexing API and updates search index
based on Drupal Page status (unpublish/deletion).

For a full description of the module, visit the
[project page](https://www.drupal.org/project/bing_indexing_api).


## Table of contents

- Requirements
- Installation
- Configuration
- Troubleshooting
- Maintainers
- Sponsor


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Register the website on Bing Webmaster Tools.
2. Verify the website in Bing Webmaster Tools to confirm you are the domain owner.
3. Get the API Key in Bing Webmaster Tools → Settings → API Access → API Key.
4. Enable and setup the module in Drupal CMS using the API Key in Configuration → Web Services → Bing Index API.
5. Make sure that BingBot is not blocked.


## Troubleshooting

To submit bug reports and feature suggestions, or to track changes:
`https://www.drupal.org/project/issues/bing_indexing_api`


## Maintainers

- Anna Demianik - [anna-d](https://www.drupal.org/u/anna-d)
- Shibin Das - [d34dman](https://www.drupal.org/u/d34dman)
- Alexander Dross - [alexanderdross](https://www.drupal.org/u/alexanderdross)
- Natalia Alves - [nataliaalves](https://www.drupal.org/u/nataliaalves)


## Sponsors

- [Factorial GmbH](https://www.drupal.org/factorial-gmbh)
- [Boehringer Ingelheim](https://www.drupal.org/boehringer-ingelheim)
